output "alb_hostname" {
  value = aws_alb.main.dns_name
}

output "cloudfront_dns_name" {
  value = aws_cloudfront_distribution.cf.domain_name
}

output "alias_domain" {
  value = local.alias_domain
}

output "bucket_name" {
  value = local.bucket_name
}