# logs.tf

# Set up CloudWatch group and log stream and retain logs for 30 days
resource "aws_cloudwatch_log_group" "web_app_log_group" {
  name              = "${local.prefix}-log-group"
  retention_in_days = 30

  tags = local.common_tags
}

/*
resource "aws_cloudwatch_log_stream" "web_app_log_stream" {
  name           = "${local.prefix}-log-stream"
  log_group_name = aws_cloudwatch_log_group.web_app_log_group.name
  depends_on     = [aws_cloudwatch_log_group.web_app_log_group]
}*/

