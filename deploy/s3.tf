locals {
  bucket_name = "${local.prefix}-my-app.${var.domain}"
}

resource "aws_s3_bucket" "oursite" {
  bucket        = local.bucket_name
  force_destroy = true
  tags = {
    Name        = "${var.project}"
    Environment = "${terraform.workspace}"
  }
}


resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.oursite.id
  acl    = "private"
}

resource "aws_s3_bucket_website_configuration" "webconfig" {
  bucket = aws_s3_bucket.oursite.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

resource "aws_cloudfront_origin_access_identity" "example" {
  comment = "origin acces key for use in cloudfront fronted app"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.oursite.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.example.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "example" {
  bucket = aws_s3_bucket.oursite.id
  policy = data.aws_iam_policy_document.s3_policy.json
}