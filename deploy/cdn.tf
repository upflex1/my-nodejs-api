locals {
  s3_origin_id = "myS3Origin"
  alias_domain = "${terraform.workspace}-my-app.${var.domain}"
}

resource "aws_cloudfront_distribution" "cf" {

  origin {
    domain_name = aws_s3_bucket.oursite.bucket_regional_domain_name
    origin_id   = local.s3_origin_id


    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.example.cloudfront_access_identity_path
    }
  }

  origin {
    origin_id   = aws_alb.main.dns_name
    domain_name = aws_alb.main.dns_name

    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "https-only"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }



  comment         = var.project
  tags            = local.common_tags
  enabled         = true
  is_ipv6_enabled = true

  aliases = [
    "${aws_s3_bucket.oursite.bucket}", local.alias_domain
  ]

  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.web_app.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1"
  }

  default_cache_behavior {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["HEAD", "GET"]


    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = local.s3_origin_id
    default_ttl            = 0
    max_ttl                = 0
    min_ttl                = 0
    compress               = true
  }

  ordered_cache_behavior {
    path_pattern           = "/api/*"
    target_origin_id       = aws_alb.main.dns_name
    viewer_protocol_policy = "redirect-to-https"

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
    forwarded_values {
      query_string = true
      headers      = ["HOST"]

      cookies {
        forward = "all"
      }
    }

  }

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 404
    response_page_path    = "/error.html"
    response_code         = 404
  }

  custom_error_response {
    error_caching_min_ttl = 0
    error_code            = 403
    response_page_path    = "/error.html"
    response_code         = 403
  }



}