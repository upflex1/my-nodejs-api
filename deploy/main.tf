terraform {
  backend "s3" {
    bucket         = "my-nodejs-api-devops-tfstate"
    key            = "my-nodejs-api.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "my-nodejs-api-tf-state-lock"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    Managedby   = "Terraform"
  }
  region = var.aws_region
}
