# my-nodejs-api



## Getting started
This is the main project of 3 repositories. And its purpose is to satisfy the requirements of a code assignment for a recruitment process.

My-NodeJS-APi is a project with very basic API endpoints built in Nodejs, with an integration with terraform.
This project in particular sets the cloud environment in AWS for the the Api, and also for the repo Frontend-app, which is described in the same group. 


## Ecosystem

### Frontend-App

[Repository](https://gitlab.com/upflex1/fronted-app)

This a simple html project. This project depends on My-nodejs-api to create the s3 bucket to upload its html files contained in the *static* folder. 


### My-nodejs-api

This project contains the api and also the whole environment, which constructs with terraform with AWS resources. 

## Test and Deploy

This project has the built-in continuous integration in GitLab.

### To run in locally
#### First: Upload the docker image to Gitlab Container Regestry.
To upload the docker image you should be positioned in the *api* directory and enter the next commands on your terminal, be aware you need your Gitlab Registry Repo, username and password. 

```
docker build --compress -t <GITLAB_REGISTRY_REPO>:<VERSION_NUMBER>
docker login -u <GITLAB_USER> -p <GITLAB_PASSWORD> registry.gitlab.com
docker push <GITLAB_REGISTRY_REPO>:<VERSION_NUMBER>
```

Now, in the deploy/variables.tf file you should change the *app_image* variable with your GITLAB_REGISTRY_REPO under the *default* property. 

#### Second: Give AWS access to pull your image from Gitlab. 
Go to your AWS account and look for Secret Manager. Click on: New Secret. 
- Select secret type: Other type of secret. 
- In Key/Value pairs add the keys: 

| Key | Value     |
| :-------- | :------- | 
| **username** | <your_gitlab_username> |
| **password** | <your_gitlab_password> |

- Click on **Next**.
- Add a name to the secret key. 
- Click on **Next** and **Store**.

Copy the ARN of the secret key. 
***
- In the *deploy/template/ecs/web_app.json.tpl* file change the *credentialsParameter* with the ARN copied from previus step. 
- In *deploy/template/ecs/task-exec-role.json* file change the Resource of the first statement. Change both resorces attributes they look like this:

```bash
"arn:aws:secretsmanager:<awsRegion>:<myAWSaccount>:secret:<secretName>-<key>",
"arn:aws:kms:*:<myAWSaccount>:key/*"
```

#### Third: Create the bucket and DynamoDB table to store the tfstate file.
1. Navigate trough S3 console and create your own bucket for this project. 
2. After create enable versioning in the bucket.
3. Go to DynamoDB and click *create table*, in the **Table Name** section write the name *my-nodejs-api-tf-state-lock*, and in **Primary Key** write *LockID*
4. In the *deploy/main.tf* file change the **bucket** attribute with your own bucket created in previous step. 

#### Fourth: Set the domain variable.
- In the *deploy/variables.tf* file, change the default value of the *domain* variable with your domain name previously issued by a certificate in AWS. (Certification process to issue the domain has to be set manually.)  

#### Fifth: Login to AWS in your cli in order to set the 3 environments variables:
- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

Considerations:
1. Your user should have a vast set of permissions to be able to create all aws resources. 
2. If you plan to create these resources in a different region than *us-east-1*, you need to change the *aws_region* variable in the *deploy/variables.tf* and also in the backgroun in *deploy/main.tf*.

### Sixth: : Construct the environment. 
Position your terminal in the root directory of this project. Enter these commands: 
```
docker-compose -f deploy/docker-compose.yml run --rm terraform init
docker-compose -f deploy/docker-compose.yml run --rm terraform plan
docker-compose -f deploy/docker-compose.yml run --rm terraform apply -auto-approve

```

These commands will execute the terraform service and will start to create the cloud environmet. 

#### Finaly: Set the CNAME's in your domain table. 
1. After the *terraform apply* command, it will be display 2 outputs: **cloudfront_dns_name** and **alias_domain**. Find them and keep the value exposed.

2. Go to your DNS table of your domain, and configure it to add this new value as:

| Type | Name     | Value                |
| :-------- | :------- | :------------------------- |
| `CNAME` | <alias_domain> | <cloudfront_dns_name> |


Now wait for the change to propagate, and try it. 

Your domain path should look like this:

*staging-my-app.yourdomain.com*

- The Frontend site it is under the path **/**. (To show the pages from the Frontend, you need to upload the static files to the S3 bucket. Find more through the Readme  in the [Repository](https://gitlab.com/upflex1/fronted-app))
- The Api App it is under the path **/api/**.

***

## Authors and acknowledgment
Luis Carlos Isaula. 
